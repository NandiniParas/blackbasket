//new change
package com.test;

/**
 * This is the class to find the biggest of three numbers
 * 
 * @author Nandini
 *
 */
public class BiggestOfThree {

	private int biggestOfThree(int num1, int num2, int num3) {

		if (num1 > num2 && num1 > num3)
			return num1;
		else if (num2 > num3)
			return num2;
		else
			return num3;

	}

	public static void main(String[] args) {

		BiggestOfThree biggest = new BiggestOfThree();
		System.out.println("The Biggest of the three numbers is "
				+ biggest.biggestOfThree(74, 156, 33));
	}

}
